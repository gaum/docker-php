##################################################################################
# PHP Version
##################################################################################
FROM php:8.1-fpm-bullseye

##################################################################################
# Set Build Args
##################################################################################
ARG APP_DEBUG

##################################################################################
# Set ENV Variables
##################################################################################
ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_HOME /tmp
ENV ACCEPT_EULA=Y
ENV APP_DEBUG=${APP_DEBUG}
ENV PATH="/usr/local/bin/phpunit:${PATH}"
ENV PHP_IDE_CONFIG="serverName=localhost"
ENV XDEBUG_SESSION="PHPSTORM"
ENV TZ=Eastern/US

##################################################################################
# Folder Structure
##################################################################################
RUN touch /var/log/maillog && chown root:root /var/log/maillog
RUN mkdir /var/log/odbc && chown www-data /var/log/odbc
RUN chmod -R 777 /var/log

##################################################################################
# Configuration
##################################################################################
ADD php/www.conf /usr/local/etc/php-fpm.d/www.conf
ADD php/zz-docker.conf /usr/local/etc/php-fpm.d/zz-docker.conf
ADD os/openssl.cnf /etc/ssl/openssl.cnf

##################################################################################
# PHP Base
##################################################################################
RUN pecl channel-update pecl.php.net
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y cron gnupg locales apt-transport-https postfix git libssh2-1 libssh2-1-dev telnet autoconf \
    ghostscript net-tools zip unzip libwebp-dev libjpeg62-turbo-dev libpng-dev libxpm-dev libfreetype6-dev libzip-dev zlib1g-dev libonig-dev \
    unixodbc-dev unixodbc odbcinst && \
    pecl install ssh2-1.3.1

RUN docker-php-ext-install pdo_mysql zip

RUN docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/
RUN docker-php-ext-install gd

RUN docker-php-ext-configure pdo_odbc --with-pdo-odbc=unixODBC,/usr/
RUN docker-php-ext-install pdo_odbc

##################################################################################
# Microsoft SQL Server Prerequisites
##################################################################################

RUN pecl install sqlsrv-5.10.0 pdo_sqlsrv-5.10.0 && docker-php-ext-enable sqlsrv pdo_sqlsrv

##################################################################################
# Redis
##################################################################################
RUN pecl install redis
RUN docker-php-ext-enable redis

##################################################################################
# Xdebug
##################################################################################
RUN if [ $APP_DEBUG = true ]; \
    then pecl install xdebug && \
    docker-php-ext-enable xdebug && \
    echo "xdebug.mode=debug" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
    echo "xdebug.client_host = host.docker.internal" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
    fi;

##################################################################################
# Postfix
##################################################################################
ADD os/main.cf /etc/postfix/main.cf
RUN echo "postmaster:    root,www-data" > /etc/aliases
RUN mkfifo /var/spool/postfix/public/pickup
RUN newaliases

##################################################################################
# Install Composer
##################################################################################
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

##################################################################################
# Service Exposure
##################################################################################
CMD service postfix start & php-fpm
